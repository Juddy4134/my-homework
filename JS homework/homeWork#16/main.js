let number = prompt("Enter number!");
while (isNaN(number) || !number){
    number = prompt("Enter number correctly!", number)
}
number = +number;

function factorial(x) {
    return (x === 1) ? x : (x * factorial(x-1));
}

console.log(factorial(number));