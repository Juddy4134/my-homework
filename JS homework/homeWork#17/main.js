let userNumber = prompt("Enter your number!");
while (isNaN(userNumber) || !userNumber) {
    userNumber = prompt("Enter your number correctly!",userNumber);
}
function fibonacci(n) {
    if (n >= 0) {
        return (n <= 1) ? n : (fibonacci(n - 1) + fibonacci(n - 2))
    }
    else if (n < 0 && !(n % 2)) {
        let a=-1;
        let b=-1;
        for (let i = -3; i >= n; i--) {
            let c = a+b;
            a=b;
            b=c;
        }
        return b;
    } else {
        n = n - n * 2;
        return fibonacci(n);
    }
 }

console.log(fibonacci(userNumber));