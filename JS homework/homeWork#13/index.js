document.body.style.backgroundColor = localStorage.getItem("background-color");
const btnContainer = document.querySelector(".theme-switch-container");
const whiteBtn = document.querySelector(".white-theme");
const blackBtn = document.querySelector(".black-theme");

btnContainer.addEventListener("click",function (event) {
    localStorage.clear();
    if (event.target === whiteBtn){
        localStorage.setItem("background-color","white");
        document.body.style.backgroundColor = localStorage.getItem("background-color");
    } else if (event.target === blackBtn){
        localStorage.setItem("background-color","black");
        document.body.style.backgroundColor = localStorage.getItem("background-color");
    }
});