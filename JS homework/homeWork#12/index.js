const images = document.querySelectorAll(".image-to-show");
let interval = setInterval(doNextElemActive, 1000, images);
let flag = true;
document.querySelector(".stop-timer").addEventListener("click", function() {
    if (flag){
        clearInterval(interval);
        flag = false;
    }
});
document.querySelector(".resume-timer").addEventListener("click",function () {
    if (!flag){
        interval = setInterval(doNextElemActive, 1000, images);
        flag = true;
    }
});

function doNextElemActive(container) {
    for (let i = 0; i < container.length; i++) {
        if (container[i].classList.contains("active")) {
            container[i].classList.remove("active");
            container[i].nextElementSibling === null ? container[0].classList.add("active") : container[i].nextElementSibling.classList.add("active");
            break;
        }
    }
}