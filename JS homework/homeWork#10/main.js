const firstPasswordIcon = document.querySelector("#firstPasswordEye");
const secondPasswordIcon = document.querySelector("#secondPasswordEye");
const firstPasswordArray = document.querySelector("#firstPassword");
const secondPasswordArray = document.querySelector("#secondPassword");
const submitButton = document.querySelector("#submitButton");
const secondLabel = document.querySelector("#confirmPasswordLabel");
const passwordForm = document.querySelector("#passwordForm");
passwordForm.addEventListener("submit",function (event) {
	event.preventDefault();
});

firstPasswordIcon.addEventListener("click",function () {
    switchIcon(firstPasswordIcon);
});
secondPasswordIcon.addEventListener("click",function () {
    switchIcon(secondPasswordIcon);
});

submitButton.addEventListener("click",function () {
    const wrong = document.querySelector(".wrong-message");
    if (firstPasswordArray.value === secondPasswordArray.value){
        if (wrong){
            wrong.remove();
        }
        alert("You are welcome!")
    } else {
        if (!wrong){
            const wrongMessage = document.createElement("p");
            wrongMessage.classList.add("wrong-message");
            wrongMessage.textContent = "Нужно ввести одинаковые значения";
            secondLabel.append(wrongMessage);
        }
    }
});

function switchIcon(icon) {
    if (icon.className === "fas fa-eye-slash icon-password"){
        icon.className = "fas fa-eye icon-password";
        const passwordArray = document.querySelector(icon.dataset.search);
        passwordArray.type = "password";
    } else if (icon.className === "fas fa-eye icon-password"){
        icon.className = "fas fa-eye-slash icon-password";
        const passwordArray = document.querySelector(icon.dataset.search);
        passwordArray.type = "text";
    }
}