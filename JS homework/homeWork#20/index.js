function isWorkCompletedInTime(speed,backlog,deadline) {
    let workInDay = 0;
    let needToDo = 0;
    const nowDate = new Date();
    let weekends = 0;
    let timeToWork;
    let timeLeft = 0;
    if (nowDate.getDay() === 6){
        weekends = new Date(nowDate.getFullYear(),nowDate.getMonth(),nowDate.getDate()+2,8);
        timeToWork = (deadline - weekends) / (60000*60*24);
    } else if (nowDate.getDate() === 0){
        weekends = new Date(nowDate.getFullYear(),nowDate.getMonth(),nowDate.getDate()+1,8);
        timeToWork = (deadline - weekends) / (60000*60*24);
    } else {
        timeToWork = (deadline - nowDate) / (60000*60*24);
    }
    speed.forEach(elem => workInDay += elem);
    backlog.forEach(elem =>needToDo += elem);

    timeToWork = timeToWork / 7 * 5;

    timeLeft = timeToWork - (needToDo / workInDay );

    console.log(timeLeft);
    console.log(needToDo);
    console.log(workInDay);
    console.log(timeToWork);

    return Math.trunc(timeLeft);
}

const firstProject = isWorkCompletedInTime([4,3,6,7],[5,6,1,8,3,4,6,1,9],new Date(2020,3,16));

console.log(firstProject);