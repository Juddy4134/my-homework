function deepClone(obj, traversedObjects = []) {
    let copy;
    // primitive types
    if (obj === null || typeof obj !== "object") {
        return obj;
    }
    // dates
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
    // arrays
    if (obj instanceof Array) {
        copy = [];
        for (let i = 0; i < obj.length; i++) {
            copy.push(deepClone(obj[i], traversedObjects.concat(obj)));
        }
        return copy;
    }
    // simple objects
    if (obj instanceof Object) {
        copy = {};
        for (let key in obj) {
            copy[key] = deepClone(obj[key], traversedObjects.concat(obj));
        }
        return copy;
    }
}

const myObj = {
    asd: "dsads",
    asdasd: "dasds",
    aqe: {
        dasasd: [1,2,3,4,{
            ads: 123,
            asd: 321,
            das: {
                asdasd: "sadasd",
                asd: [1,2,3]
            }
        }],
        asda: "dsasad",
    }
};

console.log(myObj);
const copiedObj = deepClone(myObj);
console.log(copiedObj);
myObj.aqe.dasasd.push([123,123,123]);
console.log(myObj);
console.log(copiedObj);