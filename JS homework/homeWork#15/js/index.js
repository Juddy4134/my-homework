$(".close-btn").click(function () {
    const block = $("#news");
    if (block.hasClass("active")){
        block.removeClass("active").fadeOut(600);
    } else {
        block.addClass("active").fadeIn(600);
    }
});

$(".nav-list-link").click(function () {
    $("html,body").animate({
        scrollTop: $($(this).attr("href")).offset().top
    }, {
        duration: 600,
        easing: "swing"
    });
    return false;
});

$(window).scroll(function () {
    if ($(window).scrollTop() > 400) {
        $(".scroll-up").fadeIn(600);
    } else {
        $(".scroll-up").fadeOut(600);
    }
});

$(".scroll-up").click(function (event) {
    event.preventDefault();
    $("html,body").animate({scrollTop: 0}, '300');
});
