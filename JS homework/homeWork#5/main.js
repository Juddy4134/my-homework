function createNewUser() {
    let getFirstName = prompt("Enter your name please!");
    let getSecondName = prompt("Enter your second name please!");
    let getUserBirthday = prompt("Enter you birthday in format: dd.mm.yyyy (through point");
    const birthdayArr = getUserBirthday.split(".");
    const newUser = {
        firstName: getFirstName,
        secondName: getSecondName,
        birthday: new Date(birthdayArr[2],birthdayArr[1],birthdayArr[0]),
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.secondName.toLowerCase();
        },
        getAge: function () {
            let nowDate = new Date();
            const year = this.birthday.getFullYear();
            const month = this.birthday.getMonth()-1;
            const day = this.birthday.getDay();
            let userAge = nowDate.getFullYear() - year;
            if (month > nowDate.getMonth() || (month === nowDate.getMonth() && day > nowDate.getDate())) {
                userAge--;
            }
            return userAge;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.secondName.toLowerCase() + birthdayArr[2];
        },
    };
    return newUser;
}
let firstUser = createNewUser();
console.log(firstUser);
console.log(firstUser.getLogin());
console.log(firstUser.getAge());
console.log(firstUser.getPassword());
