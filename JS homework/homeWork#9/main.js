const navItem = document.querySelectorAll(".tabs-title");
navItem.forEach(elem => {
    elem.addEventListener("click",function () {
        this.classList.add("active");
        let prev = elem.previousElementSibling;
        while (prev){
            prev.classList.remove("active");
            const prevInfo = document.querySelector(prev.dataset.set);
            prevInfo.classList.remove("active");
            prev = prev.previousElementSibling
        }
        let next = elem.nextElementSibling;
        while (next){
            next.classList.remove("active");
            const nextInfo = document.querySelector(next.dataset.set);
            nextInfo.classList.remove("active");
            next = next.nextElementSibling
        }
        const currentInfo = document.querySelector(this.dataset.set);
        currentInfo.classList.add("active");
    });
});