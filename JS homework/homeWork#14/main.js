// const navItem = document.querySelectorAll(".tabs-title");
// navItem.forEach(elem => {
//     elem.addEventListener("click", function () {
//         this.classList.add("active");
//         let prev = elem.previousElementSibling;
//         while (prev) {
//             prev.classList.remove("active");
//             const prevInfo = document.querySelector(prev.dataset.set);
//             prevInfo.classList.remove("active");
//             prev = prev.previousElementSibling
//         }
//         let next = elem.nextElementSibling;
//         while (next) {
//             next.classList.remove("active");
//             const nextInfo = document.querySelector(next.dataset.set);
//             nextInfo.classList.remove("active");
//             next = next.nextElementSibling
//         }
//         const currentInfo = document.querySelector(this.dataset.set);
//         currentInfo.classList.add("active");
//     });
// });

const navElem = $(".tabs-title");
navElem.click(function (event) {
    const dataSet = event.target.dataset.set;
    $(this).siblings().removeClass("active");
    $(this).addClass("active");
    $(dataSet).addClass("active").siblings().removeClass("active");
});

