const priceArray = document.querySelector("#priceBlock");
const priceInput =document.querySelector("#priceInput");
const valuesArray = document.querySelector("#valuesArray");
priceInput.addEventListener("blur",function () {
    let priceInputValue = +priceInput.value;
    if (priceInputValue < 0 || isNaN(priceInputValue)){
        if (document.querySelectorAll(".incorrect-message").length === 0){
            const errorMessage = document.createElement("p");
            errorMessage.textContent = "Please enter correct price";
            errorMessage.classList.add("incorrect-message");
            priceArray.append(errorMessage);
            priceInput.classList.add("false-value");
        }
        const infoSpan = document.querySelector("#infoSpan");
        if (infoSpan){
            infoSpan.remove();
        }
    } else {
        if (document.querySelectorAll(".incorrect-message")){
            const falseMessage = document.querySelectorAll(".incorrect-message");
            priceInput.classList.remove("false-value");
            falseMessage.forEach(item => item.remove());
        }
        if (priceInput.value){
            const infoArray = valuesArray.children;
            const closeSpan = document.createElement("span");
            closeSpan.textContent="X";
            priceInput.style.backgroundColor ="green";
            closeSpan.className="close";
            closeSpan.addEventListener("click", function () {
                this.closest(".span").remove();
            });
            if (infoArray.length > 0){
                const infoSpan = document.querySelector("#infoSpan");
                infoSpan.textContent = `Текущая цена: ${priceInput.value}`;
                infoSpan.append(closeSpan);
            } else {
                const createSpan = document.createElement("span");
                createSpan.id = "infoSpan";
                createSpan.textContent = `Текущая цена: ${priceInput.value}`;
                createSpan.classList.add("span");
                createSpan.append(closeSpan);
                valuesArray.append(createSpan);
                createSpan.append(document.createElement("br"));
            }
        }
    }
});
priceInput.addEventListener("click",function () {
    priceInput.style.backgroundColor ="";
});

