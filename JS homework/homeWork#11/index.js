const buttons = Array.from(document.querySelectorAll(".btn"));
window.addEventListener("keydown", function (e) {
    buttons.map(function (elem) {
        if (e.code === elem.id) {
            elem.classList.add("active");
        } else if (e.code !== elem.id) {
            elem.classList.remove("active");
        }
    });
});