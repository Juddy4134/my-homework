import React from 'react';
import {Provider} from "react-redux"
import './App.css';

import {ProductsList} from './components/ProductsList';
import {ProductCart} from './components/ProductCart';
import {store} from "./store/store"
import {NavBar} from "./components/NavBar"

function App() {
  return (
    <Provider store = {store}>
      <div className="App">
        <NavBar/>
        <ProductCart/>
        <ProductsList/>
      </div>
    </Provider>
  );
}

export default App;
