import {createStore} from "redux"
import {rootReducer} from "./reducer/root.Reduced"

export const store = createStore(rootReducer);