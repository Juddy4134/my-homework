import exchangeInitialState from "../exchange.initialState";
import {EXCHANGE_VALUES} from "../actions";


const exchangeReducer = (state = exchangeInitialState, {type}) => {

    switch (type) {
        case EXCHANGE_VALUES:
               const newState = state.map(item => {
                   return {
                       ...item,
                       active: !item.active
                   }
                })
                localStorage.setItem("currentMoney",JSON.stringify(newState))
            return newState
        default:
            return state
    }
}

export default exchangeReducer