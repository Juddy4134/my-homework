import {TOGGLE_CART} from "../actions";

const initialState = {isCartActive: false}
const toggleCart = (state = initialState, {type}) => {
    switch (type) {
        case TOGGLE_CART:
            const newState = {}
                state.isCartActive ? newState.isCartActive = false : newState.isCartActive = true
             return newState
        default:
            return state
    }
}

export default toggleCart