import {combineReducers} from "redux";
import cartList from "./products.Reducer";
import exchange from "./exchange.Reducer";
import toggleCart from "./toggleCart.Reducer"


export const rootReducer = combineReducers({
    toggleCart, exchange, cartList,
    }
)