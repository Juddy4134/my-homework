import {DELETE_FROM_CART} from "../actions"
import {ADD_TO_CART} from "../actions"
import {REMOVE_FROM_CART} from "../actions"
import {CLEAR_CART} from "../actions"
import productsInitialState from "../products.InitialState"

const changeCartList = (state = productsInitialState, action) => {
    const newState = {...state};
    const setLocalStorage = (newState) => {
        localStorage.setItem("products",JSON.stringify(newState.products))
        localStorage.setItem("totalPrice",JSON.stringify(newState.totalPrice))
    }
    let elemIndex = null
    switch (action.type) {
        case ADD_TO_CART:
            elemIndex = state.products.findIndex(elem => elem.title === action.payload.title)
            if (elemIndex >= 0 ){
                newState.products[elemIndex].count++
                newState.totalPrice += action.payload.price
            } else {
                newState.products.push({
                    title: action.payload.title,
                    count: 1,
                    img: action.payload.img,
                    price: action.payload.price
                })
                newState.totalPrice += action.payload.price
            }
            setLocalStorage(newState)
            return newState
        case REMOVE_FROM_CART:
            elemIndex = state.products.findIndex(elem => elem.title === action.payload.title)
            if(newState.products[elemIndex].count <= 1){
                newState.products.splice(elemIndex, 1)
                newState.totalPrice -= action.payload.price
            } else {
                newState.products[elemIndex].count--
                newState.totalPrice -= action.payload.price
            }
            setLocalStorage(newState)
            return newState
        case DELETE_FROM_CART:
            elemIndex = state.products.findIndex(elem => elem.title === action.payload.title)
            newState.totalPrice -= newState.products[elemIndex].price * newState.products[elemIndex].count
            newState.products.splice(elemIndex, 1)
            setLocalStorage(newState)
                return newState
        case CLEAR_CART:
                newState.totalPrice = 0
                newState.products = []
                setLocalStorage(newState)
            return newState
        default:
            return newState
    }
}

export default changeCartList