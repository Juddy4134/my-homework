import banana from "../images/banana.jpg"
import orange from "../images/orange.jpg"
import lemon from "../images/lemon.jpg"

export const initialProductState = {
    productsList: [
        {
            title: "Banana",
            price: 1.22,
            img: banana,
        },

        {
            title: "Orange",
            price: 0.5,
            img: orange,
        },

        {
            title: "Lemon",
            price: 5,
            img: lemon,
        }
    ]
}