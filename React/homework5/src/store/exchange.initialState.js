let exchangeInitialState = [
    {
        name: "$",
        value: 1,
        active: false,
    },
    {
        name: "грн",
        value: 28.3,
        active: true,
    },
]

if (localStorage.getItem("currentMoney")){
    exchangeInitialState = JSON.parse(localStorage.getItem("currentMoney"))
}

export default exchangeInitialState