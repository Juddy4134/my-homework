export const ADD_TO_CART = "ADD_TO_CART"
export const REMOVE_FROM_CART = "REMOVE_FROM_CART"
export const DELETE_FROM_CART = "DELETE_FROM_CART"
export const EXCHANGE_VALUES =  "EXCHANGE_VALUES"
export const CLEAR_CART = "CLEAR_CART"
export const TOGGLE_CART = "TOGGLE_CART"