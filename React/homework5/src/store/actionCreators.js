import {DELETE_FROM_CART} from "./actions"
import {ADD_TO_CART} from "./actions"
import {REMOVE_FROM_CART} from "./actions"
import {EXCHANGE_VALUES} from "./actions"
import {CLEAR_CART} from "./actions"
import {TOGGLE_CART} from "./actions"


export const addToCart = payload => {
    return {
        type: ADD_TO_CART, payload
    }
}

    
export const removeFromCart = payload =>{
    return {
        type: REMOVE_FROM_CART, payload
    }
}

export const deleteFromCart = payload => {
    return {
        type: DELETE_FROM_CART, payload
    }
    
}

export const exchangeValues = payload => {
    return {
        type: EXCHANGE_VALUES
    }
    
}

export const clearCart = () => {
    return {
        type: CLEAR_CART
    }
}

export const toggleCart = payload => {
    return {
        type: TOGGLE_CART
    }
}