import React from 'react'
import {useDispatch} from "react-redux"
import {clearCart,toggleCart,exchangeValues} from "../../store/actionCreators"

export const NavBar = () => {
    const dispatch = useDispatch()

    const handleClearCart = () => {
        dispatch(clearCart())
    }

    const handleToggleCart = () => {
        dispatch(toggleCart())
    }

    const handleExchangeMoney = () => {
        dispatch(exchangeValues())
    }

    return (
        <nav className="navbar navbar-inverse bg-inverse fixed-top bg-faded">
            <div className="d-flex justify-content-around">
                <button type="button" className="btn btn-primary" onClick={handleToggleCart}> Cart </button>
                <button className="btn btn-danger" onClick={handleClearCart}>Clear Cart</button>
                <button type="button" className="btn btn-primary" onClick={handleExchangeMoney}> Exchange Money </button> 
            </div>
        </nav>
    )
}