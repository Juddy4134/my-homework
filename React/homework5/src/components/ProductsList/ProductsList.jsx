import React from 'react'
import {useSelector,useDispatch,shallowEqual} from "react-redux"
import {initialProductState} from "../../store/products"
import {addToCart} from "../../store/actionCreators"

export const ProductsList = () => {
    const exchange = useSelector(({exchange}) => exchange, shallowEqual)
    const dispatch = useDispatch()
    const {productsList} = initialProductState
    
    const {name,value} = exchange.find(({active}) => active);

    const productsElements = productsList.map(item => {

        const addProductToCart = (e) => {
            e.preventDefault()
            dispatch(addToCart(item))
        }

        return (

            <div className="col">
                <div className="card" style={{width: "20rem"}}>
                    <img className="card-img-top" src={item.img} alt="Card image cap"/>
                    <div className="card-block">
                        <h4 className="card-title">{item.title}</h4>
                        <p className="card-text">Price: {(item.price * value).toFixed(2)} {name}</p>
                        <a href="#" onClick={addProductToCart} className="btn btn-primary">Add to cart</a>
                    </div>
                </div>
            </div>

            )
        }
    )

    return (
        <div className="container">
            <div className="row">
                {productsElements}
            </div>
        </div>
    )
}