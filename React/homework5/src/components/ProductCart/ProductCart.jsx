import React from 'react'
import {useSelector,useDispatch,shallowEqual} from "react-redux"
import {deleteFromCart,removeFromCart,addToCart} from "../../store/actionCreators"

export const ProductCart = () => {
    const exchange = useSelector(({exchange}) => exchange, shallowEqual)
    const cartList = useSelector(({cartList}) => cartList, shallowEqual)
    const {isCartActive} = useSelector(({toggleCart}) => toggleCart,shallowEqual)
    const dispatch = useDispatch()
    const {name,value} = exchange.find(({active}) => active);
    const totalPrice = +(cartList.totalPrice * value)

    const cartElements = cartList.products.map( item =>{
        const handleAddToCart = () => {
            dispatch(addToCart(item))
        }
        const handleRemoveFromCart = () => {
            dispatch(removeFromCart(item))
        }
        const handleDeleteFromCart = () => {
            dispatch(deleteFromCart(item))
        }
        return(

            <div className="col">
                <div className="card" style={{width: "20rem"}}>
                    <div className="card-block">
                        <h4 className="card-title">{item.title}</h4>
                        <p className="card-text">Price: {(item.price * value * item.count).toFixed(2) } {name}</p>
                        <p className="card-text">Count: {item.count}</p>
                    </div>
                    <div className="row">
                        <div className="col">
                            <button className="btn btn-primary" onClick={handleAddToCart}>+</button>
                        </div>
                        <div className="col">
                            <button className="btn btn-primary" onClick={handleRemoveFromCart}>-</button>
                        </div>
                        <div className="col">
                            <button className="btn btn-primary" onClick={handleDeleteFromCart}>Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    )
    
    if (isCartActive){
        return (
            <div className="container">
                <h4>Your cart:</h4>
                <div className="row">
                    {cartElements}
                </div>
                <h4>total price: {+totalPrice.toFixed(2)} {name}</h4>
            </div>
        )

    } else {
        return (
            null
        )
    }
    
}