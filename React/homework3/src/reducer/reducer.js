export const changeCartList = (state, action) => {
    const newState = [...state];
    const setLocalStorage = (state) => localStorage.setItem("products",JSON.stringify(state))
    switch (action.type) {
        case "ADD-TO-CART":
            const elemIndex = state.findIndex(elem => elem.name === action.elem)
            if (elemIndex >= 0 ){
                newState[elemIndex].count++
            } else {
                newState.push({
                    name: action.elem,
                    count: 1
                })
            }
            setLocalStorage(newState)
            return newState
        case "REMOVE-FROM-CART":
            if(newState[action.idx].count <= 1){
                newState.splice(action.idx, 1)
            } else {
                newState[action.idx].count--
            }
            setLocalStorage(newState)
            return newState
        case "DELETE-FROM-CART":
            newState.splice(action.idx, 1)
            setLocalStorage(newState)
                return newState
        default:
            break;
    }
}

