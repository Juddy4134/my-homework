import React from 'react';
import './App.css';
import {default as ContextProvider} from "./context/product.provider"
import {ProductsList} from './components/ProductsList';
import {ProductCart} from './components/ProductCart';

function App() {
  return (
    <ContextProvider>
      <div className="App">
        <ProductCart/>
        <ProductsList/>
      </div>
    </ContextProvider>
  );
}

export default App;
