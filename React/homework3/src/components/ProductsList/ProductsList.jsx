import React,{useContext} from 'react'
import {default as ProductContext} from "../../context/product.context"

export const ProductsList = () => {
    const {productsList,addToCart} = useContext(ProductContext)
    
    const productsElements = productsList.map(element =>
        <li>
            {element}
            <button onClick={() => addToCart(element)}>Buy</button>
        </li>
    )

    return (
        <ul>{productsElements}</ul>
    )
}