import React, { useContext } from 'react'
import {default as ProductContext} from "../../context/product.context"

export const ProductCart = () => {
    const {cart, removeFromCart, addToCart,deleteFromCart} = useContext(ProductContext)
    const cartElements = cart.map((element, index) =>
        <li>
            <span>{element.name}: {element.count}  </span>
            <button onClick={()=>{addToCart(element.name)}}>+</button>
            <button onClick={()=>{removeFromCart(index)}}>-</button>
            <button onClick={()=>{deleteFromCart(index)}}>Delete</button>
        </li>
    )
    
    return (
        <div className="selected-container">
            <h3>Выбраное:</h3>
            <ul>
                {cartElements}
            </ul>
        </div>
    )
}