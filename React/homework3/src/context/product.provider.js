import React from "react"
import {default as ProductContext} from "./product.context"
import { useReducer } from "react"
import {changeCartList} from "../reducer/reducer"


const ContextProvider = ({children})=>{
    let initialState = []
    if(localStorage.getItem("products")){
        initialState = JSON.parse(localStorage.getItem("products"))
    }
    const [cart, dispatch] = useReducer(changeCartList,initialState);

    const productsList = [
        "Манускрипт войнича", 
        "Второй том мертвых душ", 
        "Евангелие от Иуды", 
        "Настоящее завещание Франциска Оллара"
    ]

    const addToCart = product => dispatch({type: "ADD-TO-CART", elem: product})
    
    const removeFromCart = index => dispatch({type: "REMOVE-FROM-CART", idx: index})

    const deleteFromCart = index => dispatch({type: "DELETE-FROM-CART", idx: index})
    return(
        <ProductContext.Provider value={{productsList,cart,addToCart,removeFromCart,deleteFromCart}}>
            {children}
        </ProductContext.Provider>
    )
}

export default ContextProvider