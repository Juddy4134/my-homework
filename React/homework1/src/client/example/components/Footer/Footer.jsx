import React from 'react';
import {AboutUs} from "../../../../shared/components/AboutUs"
import {Logo} from "../../../../shared/components/Logo"
import {SocialNetworks} from "../../../../shared/components/SocialNetworks"
import "./style.css"

export const Footer = ()=> {
    return(
        <footer className="footer">
            <div className="container">
                <div className="footer-info">
                    <AboutUs />
                    <Logo additionalStyle="c-blue"/>
                    <SocialNetworks />
                </div>
                <div className="copyright">
                    Copyright © 2017 <span className="bold">MOVIE</span><span className="gray">RISE.</span> All Rights Reserved.
                </div>
            </div>
        </footer>
    )
}