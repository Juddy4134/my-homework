import React from 'react';
import {FilmCardsView} from "./components/FilmCardsView"
import {FilmsCategories} from "./components/FilmsCategories"
import {FilmCard} from "./components/FilmCard"
import {Subscribe} from "./components/Subscribe"
import {Loader} from "../../../../shared/components/Loader"
import "./style.css"

export const FilmsList = (props)=>{
    const filmsArr = props.list.map(item => <FilmCard {...item}/>)
    return(
        <div className="container">
            <div className="films-menu">
                <FilmsCategories />
                <FilmCardsView />
            </div>
            <div className="film-cards-container">
                {filmsArr.slice(0,12)}
            </div>
            <Subscribe />
            <div className="film-cards-container">
                {filmsArr.slice(12,16)}
            </div>
            <Loader/>
        </div>
    )
}