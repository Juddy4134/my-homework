import React from 'react';
import tableView from "./img/TableView.png"
import gridView from "./img/GridView.png"
import "./style.css"

 export const FilmCardsView = ()=> {
    return(
            <div className="cards-view">
                    <img src={gridView} alt="#" className="cards-view-img"/>
                    <img src={tableView} alt="#" className="cards-view-img"/>
            </div>
        )
}