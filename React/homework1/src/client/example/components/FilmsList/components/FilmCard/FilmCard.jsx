import React from 'react';
import "./style.css"

export const FilmCard = (props)=>{
    const {img,title,genres,mark} = props;
    return(
        <div className="film-card-container">
            <div className="film-card-img" style={{backgroundImage: `url(${img})`}}></div>
            <h4 className="film-card-title">{title}<span className="film-card-mark">{mark}</span></h4>
            <p className="film-card-genres">{genres}</p>
            
        </div>
    )
}