import React from 'react';
import "./style.css"

export const FilmsCategories = ()=>{
    return(
    <ul className="films-categories-menu">
        <li className="films-categories-item active">trending</li>
        <li className="films-categories-item">top rated</li>
        <li className="films-categories-item">new arriwals</li>
        <li className="films-categories-item">trailers</li>
        <li className="films-categories-item">coming soon</li>
        <li className="films-categories-item">genre</li>
    </ul>
    )
}