import React from 'react';
import "./style.css"

export const Subscribe = ()=> {
    return(
        <div className="subscribe-section">
            <div className="subscribe-text-container">
                <p className="subscribe-text">Receive information on the latest hit movies </p>
                <p className="subscribe-text">straight to your inbox.</p>
            </div>
            <a href="#" className="subscribe-btn">Subscribe!</a>
        </div>
    )
}