import React from 'react';
import {NavBar} from './components/NavBar'
import {CurrentFilmDescription} from './components/CurrentFilmDescription';
import "./style.css"

export const Header = (props) =>{
    const {...properties} = props;
        return (
            <header className="header"> 
                <NavBar />
                <CurrentFilmDescription {...properties}/>
            </header>
        )
}