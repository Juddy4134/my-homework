import React from 'react';
import "./style.css"

export const ActionSection = ()=>{
    return (
        <section className="action-section">
           <a href="#" className="action-section-link blue">watch now</a>
           <a href="#" className="action-section-link white ">View Info</a>
           <a href="#" className="action-section-link ">+ favorites</a>
           <i className="fa fa-ellipsis-v"></i>
        </section>
    )
}