import React from 'react';
import './../../../../../../../node_modules/font-awesome/css/font-awesome.min.css';
import "./style.css"
import {ActionSection} from "../ActionSection"

export const CurrentFilmDescription = (props)=>{
    const {title,genres,time,mark} = props;
    const genresArr = genres.map(item => <li className="current-description-item">{item}</li>)
    const markStars = new Array (Math.round(mark)).fill(<i className="fa fa-star"></i>)
    return (
        <div className="current-film-description">
            <div className="container">
            <h3 className="current-film-title">{title}</h3>
            <ul className="current-description-list">
                {genresArr}
                <li className="current-description-item">|</li>
                <li className="current-description-item">{time}</li>
            </ul>
            <div className="header-footer">
                <div className="current-mark-container">
                    {markStars} 
                    <span className="current-film-mark">{mark}</span>
                </div>
                <ActionSection />
            </div>
            </div>
        </div>
    )
}