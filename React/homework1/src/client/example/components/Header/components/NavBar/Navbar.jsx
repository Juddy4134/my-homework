import React from 'react';
import {Logo} from '../../../../../../shared/components/Logo'
import "./style.css"


export const NavBar = () =>{
        return (
            <nav className="navbar">
                <div className="container">
                <Logo />
                <div className="sign-menu">
                    <a href="#" className='sign-btn'><i className="fa fa-search"></i></a>
                    <a href="#" className="sign-btn ">sign in</a>
                    <a href="#" className="sign-btn sign-up">sign up</a>
                </div>
                </div>
            </nav>
        )
}