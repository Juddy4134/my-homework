import React from 'react';
import "./style.css"

export const AboutUs = ()=> {
    return(
        <ul className="about-us-list">
            <li className="about-us-item">About</li>
            <li className="about-us-item">Terms of Service</li>
            <li className="about-us-item">Contact</li>
        </ul>
    )
}