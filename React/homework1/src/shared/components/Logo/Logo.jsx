import React from 'react';
import "./style.css"
import "../../../app/componets/App/style.css"

export const Logo = (props)=> {
    const {additionalStyle} = props;
    const additionalClass = additionalStyle ? ` ${additionalStyle}` : "";
    return(
        <a href={"#"} className={`logo logo-main${additionalClass}`} >Movie<span className={`logo logo-sub${additionalClass}`}>rise</span></a>
    )
}