import React from 'react';
import "./style.css"

export const Loader = ()=>{
    return(
        <div className="loader-container">
            <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            <p className="loader-text">Loading</p>
        </div>
    )
}