import React from 'react';
import "./style.css"
import youtube from "./img/youtube.png"

export const SocialNetworks = ()=> {
    return(
        <div className="social-networks-container">
            <i className="fa fa-facebook-f networks-logo"></i>
            <i className="fa fa-twitter networks-logo"></i>
            <i className="fa fa-pinterest-p networks-logo"></i>
            <i className="fa fa-instagram networks-logo"></i>
            <img src={`${youtube}`} alt="#" className="networks-logo"/>
        </div>
    )
}
