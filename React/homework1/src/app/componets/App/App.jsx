import React from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import '../../../shared/styles/scss/style.scss';
import './../../../../node_modules/font-awesome/css/font-awesome.min.css';
import "./reset.css"
import "./style.css"

// if you using css deleting scss import, and uncomment this import
// import '../../../shared/styles/scss/style.css';


import {Header} from "../../../client/example/components/Header"
import {FilmsList} from "../../../client/example/components/FilmsList"
import {Footer} from "../../../client/example/components/Footer"

// images 

import nowYouSeeMe from "../../../client/example/components/FilmsList/img/nowYouSeeMe.png"
import assassinsCreed from "../../../client/example/components/FilmsList/img/assassinsCreed.png"
import fantasticBeasts from "../../../client/example/components/FilmsList/img/fantasticBeasts.png"
import tarzan from "../../../client/example/components/FilmsList/img/tarzan.png"
import doctorStrange from "../../../client/example/components/FilmsList/img/doctorStrange.png"
import capitanAmerica from "../../../client/example/components/FilmsList/img/capitanAmerica.png"
import alice from "../../../client/example/components/FilmsList/img/alice.png"
import dory from "../../../client/example/components/FilmsList/img/dory.png"
import bfg from "../../../client/example/components/FilmsList/img/bfg.png"
import independence from "../../../client/example/components/FilmsList/img/independence.png"
import iceAge from "../../../client/example/components/FilmsList/img/iceAge.png"
import moana from "../../../client/example/components/FilmsList/img/moana.png"


const props = {
    currentFilmInfo:{
        title: "the jungle book",
        genres: ["adventure","drama","family","fantasy"],
        time: "1h 46m",
        mark: "4.8",
    },
    filmCards: [
    {
        img: fantasticBeasts,
        title: "Fantastic Beasts...",
        genres: "Adventure, Family, Fantasy",
        mark: "4.7"
    },
    {
        img: assassinsCreed,
        title: "AssAssin’s Creed",
        genres: "Action, Adventure, Fantasy",
        mark: "4.2"
    },
    {
        img: nowYouSeeMe,
        title: "Now you see me 2",
        genres: "Action, Adventure, Comedy",
        mark: "4.4"
    },
    {
        img: tarzan,
        title: "The Legend of Ta...",
        genres: "Action, Adventure, Drama",
        mark: "4.3"
    },
    {
        img: doctorStrange,
        title: "Doctor Strange",
        genres: "Action, Adventure, Fantasy",
        mark: "4.8"
    },
    {
        img: capitanAmerica,
        title: "Captain America...",
        genres: "Action, Adventure, Sci-Fi",
        mark: "4.9"
    },
    {
        img: alice,
        title: "Alice Through th...",
        genres: "Adventure, Family, Fantasy",
        mark: "4.1"
    },
    {
        img: dory,
        title: "Finding Dory",
        genres: "Animation, Adventure, Comedy",
        mark: "4.7"
    },
    {
        img: bfg,
        title: "The BFG",
        genres: "Adventure, Family, Fantasy",
        mark: "3.2"
    },
    {
        img: independence,
        title: "Independence Day",
        genres: "Action, Sci-Fi",
        mark: "3.9"
    },
    {
        img: iceAge,
        title: "Ice Age: Collisio...",
        genres: "Adventure, Comedy",
        mark: "4.5"
    },
    {
        img: moana,
        title: "Moana",
        genres: "Action, Fantasy",
        mark: "4.9"
    },
    {
        img: capitanAmerica,
        title: "Captain America...",
        genres: "Action, Adventure, Sci-Fi",
        mark: "4.9"
    },
    {
        img: doctorStrange,
        title: "Doctor Strange",
        genres: "Action, Adventure, Fantasy",
        mark: "4.8"
    },
    {
        img: assassinsCreed,
        title: "AssAssin’s Creed",
        genres: "Action, Adventure, Fantasy",
        mark: "4.2"
    },
    {
        img: fantasticBeasts,
        title: "Fantastic Beasts...",
        genres: "Adventure, Family, Fantasy",
        mark: "4.7"
    }
    ]
}

export const App = () => {
    const {currentFilmInfo,filmCards} = props;
    return (
            <div className="wrapper">
                <Header {...currentFilmInfo}/>
                <FilmsList list={filmCards}/>
                <Footer/>
            </div>
    );
};
