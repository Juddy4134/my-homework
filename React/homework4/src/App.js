import React,{useReducer,useEffect} from 'react'
import {timeReducer} from './reducer'
import './App.scss';

function App() {
  const initialState = {
    isTimerLaunched: false,
    seconds: 0,
    tens: 0
  }
  const [state,dispatch] = useReducer(timeReducer,initialState)

  useEffect(()=>{
    if (state.isTimerLaunched){
      setTimeout(()=> dispatch({type: "ADD-TIME"}),10)
    }
  },[state.isTimerLaunched,state.seconds,state.tens])

  const launchTimer = () => dispatch({type: "START-TIMER"})
  const stopTimer = () => dispatch({type: "STOP-TIMER"})
  const resetTimer = () => dispatch({type: "RESET-TIMER"})

  const seconds = state.seconds < 10 ? `0${state.seconds}` : `${state.seconds}`
  const tens = state.tens < 10 ? `0${state.tens}` : `${state.tens}`

  return (
    <div className="wrapper">
      <h1>Stopwatch</h1>
      <h2>React JavaScript Stopwatch</h2>
      <p><span id="seconds">{seconds}</span>:<span id="tens">{tens}</span></p>
      <button id="button-start" onClick={()=>launchTimer()}>Start</button>
      <button id="button-stop" onClick={()=>stopTimer()}>Stop</button>
      <button id="button-reset" onClick={()=>resetTimer()}>Reset</button>
  </div> 
  );
}

export default App;
