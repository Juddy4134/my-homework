export const timeReducer = (state, action) => {
    switch (action.type) {
        case "START-TIMER":
            return {...state,isTimerLaunched: true}
        case "RESET-TIMER":
            return {...state,isTimerLaunched: false,seconds:0,tens:0}
        case "STOP-TIMER":
            return {...state,isTimerLaunched: false}
        case "ADD-TIME":
            if (!state.isTimerLaunched){
                return {...state}
            }
            if (state.tens >= 99){
                state.seconds++
                return {...state,tens: 0}
            } else{
                state.tens++
                return{...state}
            }
        default:
            break;
    }
}

