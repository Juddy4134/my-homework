import React, { Component } from 'react';
import './bootstrap.min.css';
import "./style.css"

import { AddBookForm } from '../../../client/bookList/components/AddBookForm';
import { Header } from '../../../client/bookList/components/Header';
import { BooksContainer } from '../../../client/bookList/components/BooksContainer';

export class App extends Component {
  state = {
    bookList: [],
  };

  changeEditFlag = bookId => {
    const currentBookIndex = this.state.bookList.findIndex(item => item.id === bookId);
    const newBookList = this.state.bookList.slice();
    newBookList[currentBookIndex].editFlag = !newBookList[currentBookIndex].editFlag
    this.setState({
      bookList: newBookList
    });
  };

  editBook = newBook => {
    const newBookIndex = this.state.bookList.findIndex(item => item.id === newBook.id);
    let newBookList = [...this.state.bookList];
    newBookList.splice(newBookIndex,1,newBook);
 
    this.setState({
      bookList: newBookList
    })
  }

  removeBook = bookId => {
    const newBookList = this.state.bookList.filter(item => item.id !== bookId);

    this.setState(() => {
      return {
        bookList: newBookList,
      };
    });
  };

  addBookSubmit = book => {
    const { bookList } = this.state;
    bookList.push(book);
    this.setState({
      bookList: bookList,
    });
  };

  render() {
    const { bookList } = this.state;
    const booksCount = this.state.bookList.length;
    return (
      <div className="container mt-4">
        <Header />
        <AddBookForm addBookSubmit={this.addBookSubmit}/>
    <h3 id="book-count" className="book-count mt-5">Всего книг: {booksCount}</h3>
        <BooksContainer list={bookList} removeBook={this.removeBook} editBook={this.editBook} changeEditFlag={this.changeEditFlag}/>
      </div>
    );
  }
}
