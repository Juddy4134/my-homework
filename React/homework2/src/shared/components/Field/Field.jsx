import React from 'react';
import PropTypes from 'prop-types';
import classes from './Field.module.scss';

const Field = (props) => {
  return (
   <div className={classes.root}>
       Field
   </div>
  );
};

Field.defaultProps = {
    example: '',
};

Field.propTypes = {
    example: PropTypes.string,
};

export { Field };
