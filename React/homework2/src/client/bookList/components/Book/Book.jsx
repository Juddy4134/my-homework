import React, { Component } from 'react';
import { BookUpdateForm } from '../BookUpdateForm';

class Book extends Component {
  state = {
    title: this.props.title,
    author: this.props.author,
    ISBN: this.props.ISBN,
    id: this.props.id,
  };

  render() {
    const { title, author, ISBN, id, editFlag } = this.props;

    if (editFlag) {
      return (
        <BookUpdateForm {...this.props} editBook={this.props.editBook}/>
      );
    } else {
      return (
        <tr data-id={id}>
          <td>{title}</td>
          <td>{author}</td>
          <td>{ISBN}</td>
          <td>
            <button
              className="btn btn-info btn-sm"
              onClick={this.props.changeEditFlag}>
              <i className="fas fa-edit"></i>
            </button>
          </td>
          <td>
            <button
              className="btn btn-danger btn-sm btn-delete"
              onClick={this.props.removeBook}>
              X
            </button>
          </td>
        </tr>
      );
    }
  }
}

export default Book;
