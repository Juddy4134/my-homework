import React from 'react';
import { Book } from '../Book';

export const BooksContainer = props => {
  const booksList = props.list.map(item => (
    <Book
      key={item.id}
      {...item}
      removeBook={() => props.removeBook(item.id)}
      editBook={ (book) => props.editBook(book)}
      changeEditFlag={() => props.changeEditFlag(item.id)}
    />
  ));
  return (
    <table className="table table-striped mt-2">
      <thead>
        <tr>
          <th>Title</th>
          <th>Author</th>
          <th>ISBN#</th>
          <th>Title</th>
        </tr>
      </thead>
      <tbody id="book-list">{booksList}</tbody>
    </table>
  );
};
