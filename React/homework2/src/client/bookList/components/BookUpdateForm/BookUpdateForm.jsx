import React, { Component } from 'react';

class BookUpdateForm extends Component {
  state = {
    title: this.props.title,
    author: this.props.author,
    ISBN: this.props.ISBN,
    id: this.props.id,
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  editBook = () => {
    const book = {
      title: this.state.title,
      author: this.state.author,
      ISBN: this.state.ISBN,
      id: this.state.id,
      editFlag: false
    };
    this.props.editBook(book)
  }
  render() {
    
    return (
        <tr>
          <td>
              <div className="form-group">
                  <input type="text" name="title" className="form-control" onChange={this.handleChange} value={this.state.title}/>
              </div>
          </td>
          <td>
              <div className="form-group">
                  <input type="text" name="author" className="form-control" onChange={this.handleChange} value={this.state.author}/>
              </div>
          </td>
          <td>
              <div className="form-group">
                  <input type="text" name="ISBN" className="form-control" onChange={this.handleChange} value={this.state.ISBN}/>
              </div>
          </td>                        
          <td>
            <input type="submit" value="Update" className="btn btn-primary" onClick={this.editBook}/>
          </td>
      </tr>
);
  }
}

export default BookUpdateForm;
