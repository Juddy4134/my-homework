import React, { Component } from 'react';
import { v4 } from 'uuid';

class AddBookForm extends Component {
  state = {
    title: '',
    author: '',
    ISBN: '',
    titleIsLegal: true,
    authorIsLegal: true,
    ISBNIsLegal: true,
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  isLegal = event => {
    if (!isNaN(event.target.value)){
      this.setState({
        [event.target.dataset.legal]: false,
      });
    } else {
      this.setState({
        [event.target.dataset.legal]: true,
      })
    }
  }
  showError = (isLegal) => {
      return !isLegal ? <p className="error-messege">это поле обязательно к заполнению!</p> : null
  }

  addBookSubmit = e => {
    e.preventDefault();
    const props = {
      title: this.state.title,
      author: this.state.author,
      ISBN: this.state.ISBN,
      id: v4(),
      editFlag: false,
    };
    this.props.addBookSubmit(props);
  };

    render() {
    const { title, author, ISBN } = this.state;
    return (
      <div className="row">
        <div className="col-lg-4">
          <form id="add-book-form" onSubmit={this.addBookSubmit}>
            <div className="form-group">
              <label htmlFor="title">Title</label>

              <input
                type="text"
                name="title"
                className="form-control"
                onChange={this.handleChange}
                value={title}
                required
                data-legal="titleIsLegal"
                onBlur={this.isLegal}
              />
            </div>
            {this.showError(this.state.titleIsLegal)}
            <div className="form-group">
              <label htmlFor="author">Author</label>
              <input
                type="text"
                name="author"
                className="form-control"
                onChange={this.handleChange}
                value={author}
                required
                data-legal="authorIsLegal"
                onBlur={this.isLegal}
              />
            </div>
            {this.showError(this.state.authorIsLegal)}
            <div className="form-group">
              <label htmlFor="title">ISBN#</label>
              <input
                type="text"
                name="ISBN"
                className="form-control"
                onChange={this.handleChange}
                value={ISBN}
                required
                data-legal="ISBNIsLegal"
                onBlur={this.isLegal}
              />
            </div>
            {this.showError(this.state.ISBNIsLegal)}
            <input
              type="submit"
              value="Add Book"
              className="btn btn-primary"
              onSubmit={this.addBookSubmit}
            />
          </form>
        </div>
      </div>
    );
  }
}

export default AddBookForm;
