let postCounter = 1;
let users = null;
let posts = null;

const openModal = document.querySelector(".post-adder__button");
const closeModal = document.querySelector(".post-adder__cancel-button");
const modal = document.querySelector(".post-adder__form");
const form = document.querySelector("#addPost");
const postContainer = document.querySelector(".posts-container");
const loader = document.querySelector(".loader");

form.addEventListener("submit", (e) => {
    e.preventDefault();
    const title = document.getElementById("title").value;
    const description = document.getElementById("description").value;
    loader.style.display = "flex";
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: title,
            body: description,
            userId: 1
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    });

        const post = document.createElement("blockquote");
        post.classList.add("post");
        post.innerHTML = `
                <h4 class="post__title">${title}</h4><span class="post__delete-button">X</span>
                <cite class="post__author">
                    <span class="author-name">Leanne Graham</span>
                    <span class="author-mail">Sincere@april.biz</span>
                </cite>
                <p class="post__text" >${description}</p>`;
        post.setAttribute( `data-post-id` ,`${postCounter}`);
        post.setAttribute(`data-author-id`,`1`);
        postContainer.prepend(post);
        postCounter++;
        modal.style.display = "none";
        loader.style.display = "none";
    });

openModal.addEventListener("click",() => modal.style.display = "block");
closeModal.addEventListener("click",() => modal.style.display = "none");


const requestAuthors = fetch("https://jsonplaceholder.typicode.com/users")
    .then(result => {
        return result.json();
    }).then(result => {
        users = result;
    });

const requestPosts = fetch("https://jsonplaceholder.typicode.com/posts")
    .then(result => {
        return result.json();
    })

    .then(result => {
        posts = result;
        requestAuthors.then(() => {


            posts.forEach(elem => {
                const currentUser= users[elem.userId - 1];
                const post = document.createElement("blockquote");
                post.classList.add("post");
                post.innerHTML = `
                    <h4 class="post__title">${elem.title}</h4><span class="post__delete-button">X</span>
                    <cite class="post__author">
                        <span class="author-name">${currentUser.name}</span>
                        <span class="author-mail">${currentUser.email}</span>
                    </cite>
                    <p class="post__text" >${elem.body}</p>`;
                post.setAttribute( `data-post-id` ,`${elem.id}`);
                post.setAttribute(`data-author-id`,`${elem.userId}`);
                postContainer.prepend(post);
                postCounter++;
            });
        })
            .then( () => {
                openModal.style.display = "block";

                postContainer.addEventListener("dblclick", function (e) {
                    if (e.target.classList.contains("post__text")){
                        e.target.setAttribute("contenteditable","true");
                        e.target.focus();
                        let startText = e.target.textContent;
                        e.target.addEventListener("blur",function (e) {
                            this.removeAttribute("contenteditable");
                            if (e.target.classList.contains("post__text")){
                                const parentElem = this.parentElement;
                                if (startText !== this.textContent){
                                    fetch(`https://jsonplaceholder.typicode.com/posts/${parentElem.dataset.postId}`,{
                                        method: "PUT",
                                        body: JSON.stringify({
                                            "userId": parentElem.dataset.authorId,
                                            "id": parentElem.dataset.postId,
                                            "title": parentElem.querySelector(".post__title").value,
                                            "body": this.value
                                        })
                                    })
                                }
                            }
                        });
                    }
                });

                postContainer.addEventListener("click",function (e) {
                    if (e.target.classList.contains("post__delete-button")){
                        const submit = confirm("Are you sure and you want to delete this post?");
                        if (submit){
                            const post = e.target.parentElement;
                            fetch(`https://jsonplaceholder.typicode.com/posts/${post.dataset.postId}`,{
                                method: "DELETE"
                            });
                            post.remove();
                        }
                    }
                })
            })
    });
