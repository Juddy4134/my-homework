function HamburgerException (message) {
    this.message = message;
    this.name = 'HamburgerException';
}

class Hamburger{
    constructor(size, stuffing) {
        try {
            if (!size) {
                throw new HamburgerException("no size given");
            } else if (!stuffing) {
                throw new HamburgerException("no stuffing given");
            } else {
                this.size = size;
                this.stuffing = stuffing;
                this.topping = [];
            }
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }
    }
    addTopping (topping) {
        try {
            if (this.topping.includes(topping)) {
                throw new HamburgerException("этот топпинг уже добавлен")
            } else {
                this.topping.push(topping);
            }
        } catch (e) {
            console.error(e.name + " " + e.message);
        }
    };

    removeTopping (topping) {
        try {
            const index = this.topping.indexOf(topping);
            if (index == "-1") {
                throw new HamburgerException("такого топпинга нет в списке добавленых")
            }
            this.topping.splice(index, 1);
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }
    };

    getToppings () {
        try {
            if (this.topping[0] == undefined) {
                throw new HamburgerException("В вашем гамбургере нету топпингов!");
            } else {
                return this.topping;
            }
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }
    };

    getSize () {
        return this.size;
    };

    getStuffing  () {
        return this.stuffing;
    };

    calculatePrice () {
        let sum = 0;
        for (let key in this){
            if (this[key].price){
                sum += this[key].price;
            } else if (this[key] instanceof Array){
                for (let newKey in this[key]){
                    sum += this[key][newKey].price;
                }
            }
        }
        return sum;
    };

    calculateCalories  () {
        let sum = 0;
        for (let key in this){
            if (this[key].kkal){
                sum += this[key].kkal;
            } else if (this[key] instanceof Array){
                for (let newKey in this[key]){
                    sum += this[key][newKey].kkal;
                }
            }
        }
        return sum;
    };
}

Hamburger.SIZE_SMALL = {
    name: "SIZE_SMALL",
    size:"small",
    price:50,
    kkal:20
};
Hamburger.SIZE_LARGE = {
    name: "SIZE_LARGE",
    size:"large",
    price:100,
    kkal:40
};
Hamburger.STUFFING_CHEESE = {
    name: "STUFFING_CHEESE",
    stuffing:"cheese",
    price:10,
    kkal:20
};
Hamburger.STUFFING_SALAD = {
    name: "STUFFING_SALAD",
    stuffing:"salad",
    price:20,
    kkal:5
};
Hamburger.STUFFING_POTATO = {
    name: "STUFFING_POTATO",
    stuffing:"potato",
    price:15,
    kkal:10
};
Hamburger.TOPPING_MAYO = {
    topping:"mayonese",
    price:20,
    kkal:5
};
Hamburger.TOPPING_SPICE = {
    topping:"spicy",
    price:15,
    kkal:0
};

const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
const secondHamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
secondHamburger.addTopping(Hamburger.TOPPING_MAYO);
secondHamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_SPICE).

console.log(hamburger.calculatePrice());
console.log(secondHamburger.calculatePrice());
console.log(hamburger.getToppings());
console.log(secondHamburger.calculateCalories());
console.log(hamburger.calculateCalories());
console.log(secondHamburger.getToppings());
console.log(hamburger.getStuffing());
console.log(hamburger.getStuffing());