const btn = document.querySelector("#ipSearch");
btn.addEventListener("click", threadFromSchoolboy);


async function threadFromSchoolboy() {
    const getIp = fetch("https://api.ipify.org/?format=json")
        .then(response=>response.json());

    const IP = await getIp;
	console.log(IP.ip);
    const getIpInfo = fetch(`http://ip-api.com/json/${IP.ip}?fields=continent,country,region,city,district&lang=ru`)
        .then((response)=> response.json())
        .then(response => [response.continent,response.country,response.region,response.city,response.district]);

    const params = await getIpInfo;

    this.after(createList(params));
}

function createList(...args) {
    const createListContainer = document.createElement("ul");
    args[0].forEach(elem => createListContainer.insertAdjacentHTML("beforeend",`<li>${elem}</li>`));
    return createListContainer;
}