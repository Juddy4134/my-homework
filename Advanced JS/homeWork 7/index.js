function createList() {
    return document.createElement("ul");
}

function createListItem() {
    return document.createElement("li");
}

fetch("https://swapi.dev/api/films/")
.then(response => response.json())
.then( response =>{
    const list = createList();
    console.log(response);
        response.results.forEach(elem => {
            const listItem = createListItem();
            const newList = createList();
            const episodeId = createListItem();
            const openingCrawl = createListItem();
            const episodeCharacters = createListItem();
            const episodeCharactersList = createList();
            let arr = [];

            listItem.textContent = elem.title;
            episodeId.textContent= `Episode #${elem.episode_id}`;
            openingCrawl.textContent = `Episode description : ${elem.opening_crawl}`;
            episodeCharacters.textContent = `Episode characters:`;

            elem.characters.forEach( (link) => {
                 fetch(link)
                    .then( newResult => newResult.json())
                     .then( newResult => {
                         arr.push(`<li>${newResult.name}</li>`);
                         if (arr.length === elem.characters.length){
                            episodeCharactersList.innerHTML = arr.join("");
                         }
                     })
            });

            newList.append(episodeId);
            newList.append(openingCrawl);
            episodeCharacters.append(episodeCharactersList);
            newList.append(episodeCharacters);
            listItem.append(newList);
            list.append(listItem);
        });
        document.body.prepend(list);
    });

