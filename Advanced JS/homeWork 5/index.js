function createList() {
    return document.createElement("ul");
}

function createListItem() {
    return document.createElement("li");
}

const request = new XMLHttpRequest();
request.open('GET', "https://swapi.dev/api/films/");
request.responseType = 'json';
request.send();

request.onload = function() {
    if (request.status >= 300) {
        console.log(`Error ${request.status}: ${request.statusText}`);
    } else {
        const list = createList();
        request.response.results.forEach(elem => {
            const listItem = createListItem();
            const newList = createList();
            const episodeId = createListItem();
            const openingCrawl = createListItem();
            const episodeCharacters = createListItem();
            const episodeCharactersList = createList();
	    let arr = [];

            listItem.textContent = elem.title;
            episodeId.textContent= `Episode #${elem.episode_id}`;
            openingCrawl.textContent = `Episode description : ${elem.opening_crawl}`;
            episodeCharacters.textContent = `Episode characters:`;

            elem.characters.forEach( (link) => {
                const newRequest = new XMLHttpRequest();
                newRequest.open("Get", link);
                newRequest.responseType = 'json';
                newRequest.send();

                newRequest.onload = function () {
                    if (request.status >= 300) {
                        console.log(`Error ${request.status}: ${request.statusText}`);
                    } else {
                        arr.push(`<li>${newRequest.response.name}</li>`);
                         if (arr.length === elem.characters.length){
                            episodeCharactersList.innerHTML = arr.join("");
                         }
                    }
                }
            });

            newList.append(episodeId);
            newList.append(openingCrawl);
            episodeCharacters.append(episodeCharactersList);
            newList.append(episodeCharacters);
            listItem.append(newList);
            list.append(listItem);
        });
        document.body.prepend(list);
    }
};
